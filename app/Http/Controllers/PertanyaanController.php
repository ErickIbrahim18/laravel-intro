<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        return redirect('/pertanyaan');
    }
    public function index()
    {
        $post = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function show($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);
        return redirect('/pertanyaan');
    }
    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}

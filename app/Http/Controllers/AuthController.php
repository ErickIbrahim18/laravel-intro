<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('data.register');
    }
    public function welcome()
    {
        return "Hallo";
    }
    public function welcome_post(Request $request)
    {

        $nama = $request->first_name;
        $namalengkap = $request->last_name;
        return view('data.welcome', compact('nama', 'namalengkap'));
    }
}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>
    <!--Header-->
    <div>
        <h1>Buat Account Baru!</h1>
    </div>
    <!--Isi-->
    <div>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="post">
            @csrf
            <label for="first_name">First Name:</label>
            <input type="text" name="first_name" id="first_name">
            <br>
            <br>
            <label for="last_name">Last Name:</label>
            <input type="text" name="last_name" id="last_name">
            <br>
            <br>
            <label>Gender: </label>
            <br>
            <input type="radio" name="gender" value="0" id="">Male <br>
            <input type="radio" name="gender" value="1" id="">Female <br>
            <input type="radio" name="gender" value="2" id="">Other <br>
            <br>
            <label>Nationality:</label>
            <select>
                <option value="indonesia">Indonesian</option>
                <option value="singapore">Singapore</option>
                <option value="malayysia">Malaysia</option>
                <option value="australia">Australia</option>
            </select>
            <br>
            <br>
            <label>Language Spoken:</label>
            <br>
            <input type="checkbox" name="language_spoken" value="0">Bahasa Indonesia <br>
            <input type="checkbox" name="language_spoken" value="1">English <br>
            <input type="checkbox" name="language_spoken" value="2">Other <br>
            <br>
            <label for="">Bio</label>
            <br>
            <textarea name="" id="bio" cols="30" rows="10"></textarea>
            <br>
            <input type="submit" value="Sign Up">





        </form>
    </div>

</body>

</html>